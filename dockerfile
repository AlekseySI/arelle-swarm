FROM ubuntu:18.04

WORKDIR /opt/arelle

#COPY arelle.tgz /opt/arelle
RUN apt update && \
    apt install -y curl python3-pip libxml2-dev libxslt1.dev
 
RUN curl https://arelle.org/download/1210/ | tar xzf -

COPY requirements.txt /opt/arelle

RUN pip3 install -r requirements.txt; \
    apt remove -y python3-pip libxml2-dev libxslt1.dev

EXPOSE 8099

CMD [ "./arelleCmdLine", "--webserver", "0.0.0.0:8099" ] 
